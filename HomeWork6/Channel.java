package com.company;

import java.util.Random;


public class Channel {
    private TV tv;
    private String channelName;
    private final Random random = new Random();


    //массив программ, которые есть показывают на этом канале
    private Program[] programs;
    private int programCount;

    public Channel (String channelName) {
        this.channelName = channelName;

    }

    public String getChannelName() {

        return channelName;
    }

    public void setPrograms(Program[] programs) {

        this.programs = programs;
    }

    public Program getRandomProgram() {

        return programs[random.nextInt(programs.length)];
    }
}
