package com.company;

//Телевизор
public class TV {
    //поля - компания-производитель и модель телевизора
    private String company;
    private String model;
    //ссылка на пульт, который управляет телевизором
    private RemoteController remoteController;


    //массив каналов, которые будут показываться в этом телевизоре
    private Channel[] channels;
    private int channelCount;

    //конструктор - принимает только модель и номер, потому что изначально
    //мы не знаем....
    public TV (String company, String model) {
        this.company = company;
        this.model = model;
    }

    //метод, которы
    public void setRemoteController(RemoteController remoteController)  {

        this.remoteController = remoteController;
    }

    public void setChannels(Channel[] channels) {

        this.channels = channels;
    }

    public Program switchChannel (int index) {
        return channels[index].getRandomProgram();
    }
}
