package com.company;


public class Main {

    public static void main(String[] args) {
    //телевизор
        TV tv = new TV ("Sony", "TauGiga");
        RemoteController controller = new RemoteController();
        tv.setRemoteController(controller);
        controller.setTv(tv);

    //каналы
        Channel channel1 = new Channel("Sports");
        Channel channel2 = new Channel("Interactive");
        Channel channel3 = new Channel("Anonymus");
        Channel channel4 = new Channel("Variative");
        Channel channel5 = new Channel("Lazy");
        Channel channel6 = new Channel("Strong");
        Channel channel7 = new Channel("Weak");
        Channel channel8 = new Channel("Important");
        Channel channel9 = new Channel("Sex");
        Channel channel10 = new Channel("Disputive");
        Channel[] channels = {channel1, channel2, channel3, channel4, channel5, channel6, channel7, channel8, channel9, channel10};
        tv.setChannels(channels);

        //программы
        Program program1 = new Program("Sport prog");
        Program program2 = new Program("Music prog");
        Program program3 = new Program("Adverts1");
        Program[] programsAtChannel1 = {program1, program2, program3};
        channel1.setPrograms(programsAtChannel1);

        Program program4 = new Program("Adverts2");
        Program program5 = new Program("Films prog");
        Program program6 = new Program("News prog");
        Program[] programsAtChannel2 = {program4, program5, program6};
        channel2.setPrograms(programsAtChannel2);

        Program program7 = new Program("Food prog");
        Program program8 = new Program("Larry King's show");
        Program program9 = new Program("GudNite Pr0GramI8ts");
        Program[] programsAtChannel3 = {program7, program8, program9};
        channel3.setPrograms(programsAtChannel3);

        //сценарий работы с созданными объектами
        System.out.println(controller.switchChannel(2));

        }

    }

