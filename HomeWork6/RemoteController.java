package com.company;

public class RemoteController {
    private TV tv;

    public void setTv(TV tv) {

        this.tv = tv;
    }

    public Program switchChannel(int index) {

        return tv.switchChannel(index);
    }
}
