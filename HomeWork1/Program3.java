import java.util.Scanner;

class Program3 {
	public static void main (String[]args) {
		final int BASE = 10;
		int product = 1;
		Scanner scanner = new Scanner(System.in);
		int currentNumber = scanner.nextInt();

		while (currentNumber != 0) {
			int digitsSum = 0;
			int copyNumber = currentNumber;
			while (copyNumber != 0) {
				digitsSum += copyNumber % BASE;
				copyNumber /= BASE;
			}
			boolean isPrime = true;
			for (int factor = 2; factor < digitsSum; factor++) {
				if (digitsSum % factor == 0) {
					isPrime = false;
					break;
				}
			}
			if (isPrime) {
				product *= currentNumber;
			}
			currentNumber = scanner.nextInt();
		}
		System.out.println(product);
	}
}