public class Main {

        public static void main(String[] args) {


                User user = new User.Builder()
                        .firstName("Marsel")
                        .lastName("Sidikov")
                        .age(26)
                        .isWorker(true)
                        .build();

                System.out.println(user);
        }
}