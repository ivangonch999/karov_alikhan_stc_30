import java.util.Scanner;

public class Day4_Task1
{
    public static class Chekin {

        public static void main(String[] args) {
            Scanner in = new Scanner(System.in);
            System.out.print("ЧИСЛО: ");
            int num = in.nextInt();

            while (num != 1 && num % 2 == 0) {
                num /= 2;
            }

            System.out.println(num == 1 ? "ДА" : "НЕТ");
        }
    }

}
