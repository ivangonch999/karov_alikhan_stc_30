package inno;

public class Main {
    public static void main(String[] args) {
        Map<String, String> map = new MapPrimitiveImpl<>();

        map.put("Марсель", "Сидиков");
        map.put("Виктор", "Евлампьев");
        map.put("Айрат", "Мухутдинов");
        map.put("Даниил", "Вдовинов");

        map.put("Марсель", "Гудайдиев");

        System.out.println(map.get("Даниил"));
        System.out.println(map.get("Виктор"));

    }
}
