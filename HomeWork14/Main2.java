package inno.hashes;

public class Main {

     public static int hashCode (String s) {
        int h = 0;
        char value[] = s.toCharArray();

            for (int i = 0; i < value.length; i++) {
                h = 31 * h + value[i];
            }
        return h;
    }

    public static int myHashCode(String string) {
        char array[] = string.toCharArray();
        int code = 0;
        for (int i = 0; i < array.length; i++) {
            code += array[i];
        }
        return code;
    }

    public static void main(String[] args) {
        String hello = "Hello!";
        System.out.println(hello.hashCode());
        System.out.println(hashCode(hello));

    }


}
