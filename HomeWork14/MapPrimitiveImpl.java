package inno;

public class MapPrimitiveImpl<K, V> implements Map<K, V> {
    private static final int DEFAULT_SIZE = 10;
    private static class MapEntry<K, V> {
        K key;
        V value;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    MapEntry<K, V> entries[];

    private int count;

    public MapPrimitiveImpl() {
        this.entries = new MapEntry[DEFAULT_SIZE];
        this.count = 0;
    }


    //например, кладем map.put("Марсель", "Сидиков");
    //а если второй раз map.put("Марсель", "Гудайдиев");
    @Override
    public void put(K key, V value) {
        MapEntry<K, V> entry = new MapEntry<>(key, value);

        for (int i = 0; i < count; i++) {
            // если нашли значение, которое уже было под этим ключом
            if (entries[i].key.equals(key)) {
                //заменяем это значение
                entries[i] = entry;
                return;
            }
        }
        this.entries[count] = entry;
        count++;


    }

    @Override
    public V get(K key) {
        for (int i = 0; i < count; i++) {
            // если нашли совпадающий ключ
            if (entries[i].key.equals(key)) {
                return  entries[i].value;

            }
        }

        return null;
    }
}
