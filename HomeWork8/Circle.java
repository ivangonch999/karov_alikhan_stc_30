public class Circle extends Figures {

    private final double radius;

    public Circle(double r){
        this.radius = r;
    }

    @Override
    public void draw() {
        System.out.println("Рисуем Круг");
    }

    @Override
    public double getArea(){
        // Вычисляем площадь
        return Math.PI*this.radius*this.radius;
    }


}