public interface Scalable {

    //неявно public, static и final
    public String LABEL=">>> Масштабируемый";

    //методы интерфейса неявно abstract и public
    void draw();

    double getArea();


}