

public class Main {
    public static void main(String[] args) {

        //реализуем интерфейс
        Scalable shape = new Circle(10);
        shape.draw();
        System.out.println("Площадь = " + shape.getArea());

        //прямоугольник
        shape = new Rectangle(10, 10);
        shape.draw();
        System.out.println("Площадь = " + shape.getArea());

        //эллипс который не-эллипс
        shape = new Ellipse(22);
        shape.draw();
        System.out.println("Площадь = " + shape.getArea());

        //тут есть вопросы по расчету площади, почему еще в полях нельзя сразу присвоить значения,
        //а нужно идти в конструктор. А?
        shape = new Box(777);
        shape.draw();
        System.out.println("Площадь = " + shape.getArea());

        //а на кой нам shape? можно же box
        Box fuckInterfaces = new Box(666);
        fuckInterfaces.draw();
        System.out.println("А че, так можно было? " + shape.getArea());

        //реализация интерфейсов
        Movable movable = new Box(77);
        movable.move();
        System.out.println(movable.LABEL);

        System.out.println(shape.LABEL);

    }


    }

