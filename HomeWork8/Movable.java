public interface Movable {

    //неявно public, static и final
    String LABEL=">>> Перемещаемый";

    //методы интерфейса неявно abstract и public
    void move();

}