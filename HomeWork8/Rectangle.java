public class Rectangle extends Figures {

    private final double width;
    private final double height;

    public Rectangle(double w, double h){
        this.width=w;
        this.height=h;
    }
    @Override
    public void draw() {
        System.out.println("Рисуем прямоугольник");
    }

    @Override
    public double getArea() {
        return this.height*this.width;
    }

}