public class Ellipse extends Figures {

    private final double smallHemis;
    private final double greatHemis;

    public Ellipse(double greatHemis, smallHemis){

        this.smallHemis = smallHemis;
        this.greatHemis = greatHemis;
    }

    @Override
    public void draw() {
        System.out.println("Рисуем эллипс");
    }

    @Override
    public double getArea(){
        // площадь нннада
        return Math.PI*this.smallHemis*this.greatHemis;
    }

    @Override
    public void move() {

    }
}
