package ru.karov.game.models;

import java.util.Objects;


public class Player {
//    private Long id;
    private String ip;
    private String name;
    private Integer points;
    private Integer maxWinsCount;
    private Integer maxLosesCount;

    public Player(String ip, String name, Integer points, Integer maxWinsCount, Integer maxLosesCount) {
        this.ip = ip;
        this.name = name;
        this.points = points;
        this.maxWinsCount = maxWinsCount;
        this.maxLosesCount = maxLosesCount;
    }

//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getMaxWinsCount() {
        return maxWinsCount;
    }

    public void setMaxWinsCount(Integer maxWinsCount) {
        this.maxWinsCount = maxWinsCount;
    }

    public Integer getMaxLosesCount() {
        return maxLosesCount;
    }

    public void setMaxLosesCount(Integer maxLosesCount) {
        this.maxLosesCount = maxLosesCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(ip, player.ip) &&
                Objects.equals(name, player.name) &&
                Objects.equals(points, player.points) &&
                Objects.equals(maxWinsCount, player.maxWinsCount) &&
                Objects.equals(maxLosesCount, player.maxLosesCount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip, name, points, maxWinsCount, maxLosesCount);
    }
}
