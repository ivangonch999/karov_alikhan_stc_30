package ru.karov.game;

package ru.karov.game.app;

import ru.karov.game.dto.StatisticDto;
import ru.karov.game.repository.*;
import ru.karov.game.services.GameService;
import ru.karov.game.services.GameServiceImpl;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        PlayersRepository playersRepository = new PlayersRepositoryMapImpl();
        GamesRepository gamesRepository = new GamesRepositoryListImpl();
        ShotsRepository shotsRepository = new ShotsRepositoryFileImpl("shots_db.txt",
                "shots_sequence.txt");
        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);

        Scanner scanner = new Scanner(System.in);

        String first = scanner.nextLine();
        String second = scanner.nextLine();
        Random random = new Random();

        Long gameId = gameService.startGame("127.0.0.1", "127.0.0.2", first, second);
        String shooter = first;
        String target = second;
        int i = 0;
        while (i < 10) {

            System.out.println(shooter + " делайте выстрел в " + target);
            scanner.nextLine();

            int success = random.nextInt(2);

            if (success == 0) {
                System.out.println("Успешно!");
                gameService.shot(gameId, shooter, target);
            } else {
                System.out.println("Промах!");
            }

            String temp = shooter;
            shooter = target;
            target = temp;
            i++;
        }
        // TODO: Необходимо вывести информацию
        StatisticDto statistic = gameService.finishGame(gameId);
        System.out.println(statistic);
    }
}
