package ru.karov.game.repository;

import ru.karov.game.models.Game;


public interface GamesRepository {
    void save(Game game);

    Game findById(Long gameId);

    void update(Game game);
}
