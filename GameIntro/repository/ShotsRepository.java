package ru.karov.game.repository;

import ru.karov.game.models.Shot;

public interface ShotsRepository {
    void save(Shot shot);
}
