import java.util.Scanner;
import java.util.Arrays;

class Day2_Task2  {
	public static void main (String args[]) {

		Scanner scanner = new Scanner (System.in);
		int n = scanner.nextInt ();
		int numbers[] = new int [n];
		
		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = scanner.nextInt(); 
		}

		for (int i = 0; i < numbers.length / 2; i++) {
			int temp = numbers[i];
			numbers[i] = numbers[n-i-1];
			numbers[n-i-1] = temp;
		}
		

		System.out.println (Arrays.toString(numbers));


      }


}

		