
public class Main {
    public static void main(String[] args) {

        //развернуть число (задание 1)
        NumbersProcess reverseNumber = Utils::reverseNumber;
        System.out.println(reverseNumber.process(786056));

        //убрать нули (задание 2)
        NumbersProcess removeZeros = Utils::removeZeroes;
        System.out.println(removeZeros.process(833600005));

        //заменить нечетные на ближайшие четные снизу (задание 3)
        NumbersProcess swapOdds = Utils::swapOdds;
        System.out.println(swapOdds.process(834578305));

        //развернуть исходную строку (задание 4)
        StringProcess reverseString = Utils::reverseString;
        System.out.println(reverseString.process("Alibaba"));

        //убрать все числа из строки (задание 5)
        StringProcess removeDigits = Utils::removeDigits;
        System.out.println(removeDigits.process("COLD833600005RAY"));

        //сделать все буквы большими (задание 6)
        StringProcess toUpperCase = String::toUpperCase;
        System.out.println(toUpperCase.process("Hello, Bunny"));


    }




}



