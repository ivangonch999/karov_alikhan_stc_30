public class Utils {
    static int removeZeroes(int number) {
        var string = String.valueOf(number);
        string = string.replace("0", "");
        return Integer.parseInt(string);
    }

    static String removeDigits(String stringa) {
        String s = stringa;
        s = s.replaceAll("[^A-Za-zА]", "");
        return s;
    }

    static String reverseString(String str) {
        var index = str.length() - 1;
        var reversedString = "";

        while (index >= 0) {
            var currentChar = str.charAt(index);
            reversedString = reversedString + currentChar;
            index = index - 1;
        }

        return reversedString;
    }

    static int reverseNumber(int process) {
        int reverse = 0;
        while (process != 0) {
            reverse = reverse * 10;
            reverse = reverse + process % 10;
            process = process / 10;
        }
        return reverse;
    }

    static int swapOdds(int process) {
        int temp = process;
        temp = Integer.parseInt(Integer.toString(temp).replaceAll("3", "2").replaceAll("5", "4").replaceAll("7", "6").replaceAll("9", "8"));
        return temp;
    }
}
