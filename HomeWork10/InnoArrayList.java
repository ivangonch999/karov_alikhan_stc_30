public class InnoArrayList implements InnoList {

    private static final int DEFAULT_SIZE = 10;

    private int elements[];

    private int count;

    public InnoArrayList() {
        this.elements = new int[DEFAULT_SIZE];
        this.count = 0;
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        } else {
            return -1;
        }
    }

    @Override
    public void insert(int index, int element) {
        // создается вр. пустой массив большим размером чем изначальный
        int[] temp = new int[elements.length + 1];
        if (index < 0 || index > elements.length - 1) {
            System.err.println("Incorrect index");
        } else {
            int i;
            // копируем значения во временный массив до переданного значнения
            // (index), а потом...
            for (i = 0; i < index; i++) {
                temp[i] = elements[i];
            }
            // присваиваем значение нужной нам ячейки
            temp[index] = element;
            
            // дозаполняем временный массив в обход удаленной выше ячейки
            for (int k = i; k < elements.length; k++) {
                temp[k + 1] = elements[k];
            }
            // и снова меняем ссылки, и дарим тэмп сборщику мусора
            elements = temp;
        }
        
        
    }

    @Override
    public void addToBegin(int element) {
        // создается пустой временный массив, с размером больше чем изн. массив
        int[] temp = new int[elements.length + 1];
        // вставляем в нулевую ячейку передавемое значение
        temp[0] = element;
        // а начиная с уже индекса первого (после 0) пробегаемся по всему
        // массиву дозаполняя ячейки временного изначальными минус 1
        for (int i = 1; i <= elements.length; i++) {
            temp[i] = elements[i - 1];
        }
        //ссылку изначального массива напрявляем на участок памяти куда ссылается
        // временный массив. Теперь элементс ссылается на новый массив. Темп забирает
        // сброщик мусора
        elements = temp;
    }

    @Override
    public void removeByIndex(int index) {
        // создается вр. пустой массив меньшем размера чем изначальный
        int[] temp = new int[elements.length - 1];
        if (index < 0 || index > elements.length - 1) {
            System.err.println("Incorrect index");
        } else {
            int i;
            // копируем значения во временный массив до переданного значнения
            // (index), а потом...
            for (i = 0; i < index; i++) {
                temp[i] = elements[i];
            }
            // дозаполняем временный массив в обход удаленной выше ячейки
            for (int k = i; k < elements.length - 1; k++) {
                temp[k] = elements[k + 1];
            }
            // и снова меняем ссылки, и дарим тэмп сборщику мусора
            elements = temp;
        }
    }

    @Override
    public void add(int element) {
        // если список переполнен
        if (count == elements.length) {
            resize();
        }

        elements[count++] = element;
    }

    private void resize() {
        // создаем новый массив в полтора раза больший
        int newElements[] = new int[elements.length + elements.length / 2];
        // копируем из старого массива все элементы в новый
        for (int i = 0; i < count; i++) {
            newElements[i] = elements[i];
        }
        // устанавливаем ссылку на новый массив
        this.elements = newElements;
    }

    @Override
    public void remove(int element) {
        // пробегаем весь массив
        for (int i = 0; i < elements.length; i++) {
            // если текущий элемент совпал с запрошенным
            if (elements[i] == element) {
                //в этот индекс пишем ноль
                elements[i] = 0;
            }
        }
    }

    @Override
    public boolean contains(int element) {
        // пробегаем весь массив
        for (int i = 0; i < elements.length; i++) {
            // если текущий элемент совпал с запрошенным
            if (elements[i] == element) {
                //возвращаем тру
                return true;
            }
         }
        // или фолс
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public InnoIterator iterator() {
        // возвращаем новый экземпляр итератора
        return new InnoArrayListIterator();
    }

    // внутренний класс позволяет инкапсулировать логику одного класса внутри класса
    private class InnoArrayListIterator implements InnoIterator {
        // текущая позиция итератора
        private int currentPosition;

        @Override
        public int next() {
            // берем значение под текущей позицией итератора
            int nextValue = elements[currentPosition];
            // увеличиваем позицию итератора
            currentPosition++;
            // возвращаем значение
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            // если текущая позиция не перевалила за общее количество элементов - можно дальше
            return currentPosition < count;
        }
    }
}
