import java.util.Arrays;
import java.util.Scanner;

public class Day3_Task1 {

    public static void main (String args[]) {
        Scanner scanner = new Scanner(System.in);


        System.out.println("Введите размер массива:");
        int size = scanner.nextInt();
        int[] array = new int[size];


        double sum = 0, arf;


        System.out.println("Вводите числа:");
        for (int i = 0; i < size; i++) {
            array[i] = scanner.nextInt();
        }


        for(int num : array) {
            sum = sum + num;
        }
        System.out.println("Сумма элементов массива равна: " + sum);


        for (int i = size-1; i >=0; i--) {
            System.out.println("Разворот массива: " + array[i]);
        }


        arf = sum / size;
        System.out.println("Среднее арифметическое массива: " + arf);


        //вычисляем мин и макс
        int[] arrayOriginal = Arrays.copyOf(array, array.length); //копия, чтобы вывести на экран массив в изначальном виде
        int minV = array[0];
        int maxV = array[0];
        int tempMin = 0, tempMax = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i] < minV) {
                minV = array[i];
                tempMin = i;
            }
            if (array[i] > maxV) {
                maxV = array[i];
                tempMax = i;
            }
        }
        array[tempMin] = maxV;
        array[tempMax] = minV;


        // код из инета, массив в число - не до конца понял,    заменить на схема горнера
        int result = 0;
        for (int i = array.length - 1, n = 0; i >= 0; --i, n++) {
            int pos = (int)Math.pow(10, i);
            result = result + array[n] * pos;
        }

        //пошли пузырьки
        boolean isSorted = false;
        int buf;
        while(!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length-1; i++) {
                if(array[i] > array[i+1]){
                    isSorted = false;
                    buf = array[i];
                    array[i] = array[i+1];
                    array[i+1] = buf;
                }
            }
        }

        //вывод
        System.out.println("Мин значение: " + minV);
        System.out.println("Макс значение: " + maxV);
        System.out.println("Изначально: " + Arrays.toString(arrayOriginal));
        System.out.println("Замена мин<->макс: " + Arrays.toString(array));
        System.out.println("Массив в число: " + result);
        System.out.println("После сортировки: " + Arrays.toString(array));

    }


}




